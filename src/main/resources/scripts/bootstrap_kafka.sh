#!/bin/bash
#should be done as kafka user
/usr/local/kafka/bin/kafka-topics.sh --zookeeper ${zookeeper-hosts} --create --topic documents --partitions ${samza.tasks.number} --replication-factor 2
/usr/local/kafka/bin/kafka-topics.sh --zookeeper ${zookeeper-hosts} --create --topic queries --partitions ${samza.tasks.number} --replication-factor 2
/usr/local/kafka/bin/kafka-topics.sh --zookeeper ${zookeeper-hosts} --create --topic matches1 --partitions ${samza.tasks.number} --replication-factor 2