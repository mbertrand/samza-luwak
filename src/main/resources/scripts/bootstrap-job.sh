#!/bin/bash

echo "Bootstraping Samza Jobs"
echo "Transfering tarball to hdfs..."
sudo -u hdfs hdfs dfsadmin -safemode leave
sudo -u hdfs hdfs dfs -mkdir -p /user/samza
sudo -u hdfs hdfs dfs -put /jobs/samza-luwak/samza-luwak-1.0-SNAPSHOT-dist.tar.gz /user/samza/samza-luwak-1.0-SNAPSHOT-dist.tar.gz

echo "creating kafka topics on broker 1"
ssh root@kafka-broker-1 'bash -s' < /jobs/samza-luwak/etc/bootstrap_kafka.sh 

echo "launching samza job"
cd /jobs/samza-luwak/bin
sudo -u hdfs  ./launch_job.sh
