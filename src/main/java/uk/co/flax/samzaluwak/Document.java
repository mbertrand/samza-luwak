/**
 * 
 */
package uk.co.flax.samzaluwak;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author pcaparroy
 *
 */
public class Document {

	@JsonProperty
	private String content;
	
	@JsonProperty
	List<String> queryIds;

	
	
	
	public Document(String content) {
		this(content,null);
	}

	@JsonCreator
	public Document(@JsonProperty("content")String content,
			@JsonProperty("queryIds")List<String> queryIds) {
		super();
		this.content = content;
		this.queryIds = queryIds;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @return the queryIds
	 */
	public List<String> getQueryIds() {
		return queryIds;
	}

	/**
	 * @param queryIds the queryIds to set
	 */
	public void setQueryIds(List<String> queryIds) {
		this.queryIds = queryIds;
	}
	
	

}
