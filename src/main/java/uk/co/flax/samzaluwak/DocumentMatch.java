/**
 * 
 */
package uk.co.flax.samzaluwak;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import uk.co.flax.luwak.Matches;
import uk.co.flax.luwak.QueryMatch;

/**
 * @author pcaparroy
 *
 */
public class DocumentMatch {

	@JsonProperty
	private List<String> queryIds;
	@JsonProperty
	private Map<String,String> docFields;
	
	
	@JsonCreator
	public DocumentMatch(@JsonProperty("queryIds")List<String> queryIds,@JsonProperty("docFields") Map<String, String> docFields) {
		super();
		this.queryIds = queryIds;
		this.docFields = docFields;
	}

	/**
	 * @return the queryMatches
	 */
	public List<String> getQueryIds() {
		return queryIds;
	}

	/**
	 * @return the docFields
	 */
	public Map<String, String> getDocFields() {
		return docFields;
	}
	
	
	
}
