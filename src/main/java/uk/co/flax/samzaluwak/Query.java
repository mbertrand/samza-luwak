/**
 * 
 */
package uk.co.flax.samzaluwak;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author pcaparroy
 *
 */
public class Query {
	
	@JsonProperty
	private String query;

	@JsonCreator
	public Query(@JsonProperty("query")String query) {
		super();
		this.query = query;
	}

	/**
	 * @return the query
	 */
	public String getQuery() {
		return query;
	}

	

}
