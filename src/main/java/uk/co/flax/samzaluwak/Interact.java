package uk.co.flax.samzaluwak;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.zip.GZIPInputStream;

import org.apache.commons.io.IOUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import uk.co.flax.luwak.MonitorQuery;
import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;

/**
 * Copyright (c) 2014 Lemur Consulting Ltd.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class Interact {

    public static void main(String... args) throws IOException {

        Properties props = new Properties();
        props.setProperty("metadata.broker.list", "kafka1:9092");
        props.setProperty("serializer.class", "kafka.serializer.StringEncoder");
        ProducerConfig config = new ProducerConfig(props);

        Producer<String, String> producer = new Producer<>(config);
     // load the queries
        String dirPath = args[0];
        File dir = new File(dirPath);
        
        if(dirPath.endsWith("queries")){
        	
        	injectQueries(dir, producer);
        	
        }else if(dirPath.endsWith("docs")){
        	
        	injectDocs(dir, producer);
        }

    }
    
    private static void injectQueries(File query_dir, Producer producer) throws IOException{
    	File[] query_files = query_dir.listFiles(new FileFilter() { 
    		public boolean accept(File f) { return f.getName().endsWith(".txt"); }
    	});
    	
		ObjectMapper mapper = new ObjectMapper();
	
    	int count = 0;
    	
    	for (File query_file : query_files) {
    		
    		FileInputStream fis = new FileInputStream(query_file);
    		String query_text = IOUtils.toString(fis);
    		
    		Query query = new Query("+located");
    		
    		fis.close();
    		String query_id = query_file.getName();
    		query_id = query_id.substring(0, query_id.length() - 4);
    		String jsonQuery = mapper.writeValueAsString(query);
        	
    		sendQueryUpdate(query_id, jsonQuery, producer);
        	
    		count++;
        	if (count % 1000 == 0) {
        		System.out.println("injected " + count + " queries");
        	}
    	}
      
    }
    
    private static void injectDocs(File doc_dir, Producer producer) throws IOException{
    	
    	File[] doc_files = doc_dir.listFiles(new FileFilter() { 
    		public boolean accept(File f) { return f.getName().endsWith(".gz"); }
    	});

		ObjectMapper mapper = new ObjectMapper();
    	int count = 0;
    	for (File doc_file : doc_files) {
    		FileInputStream fis = new FileInputStream(doc_file);
    		String doc_text = IOUtils.toString(new GZIPInputStream(fis));
    		fis.close();
    		Document doc = new Document(doc_text);
    		
    		String jsonDoc = mapper.writeValueAsString(doc);
    	System.out.println(jsonDoc);
        	sendDocument(doc_file.getName(), jsonDoc, producer);
        	count++;
        	if (count % 1000 == 0) {
        		System.out.println("injected " + count + " docs");
        	}
    	}
    	System.out.println("injected " + count + " docs");
    }

    private static void sendQueryUpdate(String id,String query, Producer<String, String> producer) {

        KeyedMessage<String, String> message
                = new KeyedMessage<>(MonitorTask.QUERIES_STREAM, id, query);
        producer.send(message);

    }

    private static void sendDocument(String id,String doc, Producer<String, String> producer) {

       
       // for (int i = 0; i < MatchRecombinerTask.QUERY_PARTITIONS; i++) {
            KeyedMessage<String, String> message =
                    new KeyedMessage<>(MonitorTask.DOCS_STREAM, id, doc);
            producer.send(message);
        //}

    }

}
