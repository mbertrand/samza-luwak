package uk.co.flax.samzaluwak;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.samza.config.Config;
import org.apache.samza.storage.kv.Entry;
import org.apache.samza.storage.kv.KeyValueIterator;
import org.apache.samza.storage.kv.KeyValueStore;
import org.apache.samza.system.IncomingMessageEnvelope;
import org.apache.samza.task.InitableTask;
import org.apache.samza.task.MessageCollector;
import org.apache.samza.task.StreamTask;
import org.apache.samza.task.TaskContext;
import org.apache.samza.task.TaskCoordinator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;

/**
 * Copyright (c) 2014 Lemur Consulting Ltd.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class MatchRecombinerTask implements StreamTask, InitableTask {

    private static final Logger logger = LoggerFactory.getLogger(MatchRecombinerTask.class);

    public static final int QUERY_PARTITIONS = 2;
    public static final String QUERIES_STREAM = "queries";
    public static final String MATCHES_STREAM = "matches1";
    private ObjectMapper mapper;

    private KeyValueStore<String, String> store;

  //  public static final SystemStream MATCHES_STREAM = new SystemStream("kafka", "combinedmatches");

    @Override
    public void process(IncomingMessageEnvelope message, MessageCollector collector, TaskCoordinator taskCoordinator) throws Exception {
    	String stream = message.getSystemStreamPartition().getStream();
        switch (stream) {
            case QUERIES_STREAM:
                logger.info("Adding new query for partition {}: {}", message.getSystemStreamPartition().getPartition().getPartitionId(), message.getMessage());
               
                store.put((String) message.getKey(),(String) message.getMessage() );
                break;
            case MATCHES_STREAM:
            	try {
            		String key = (String) message.getKey();
                    String docMatchJson = (String) message.getMessage();
                    Document matchedDoc = mapper.readValue(docMatchJson, Document.class);
                   /* Map<String,String> docFields = (Map<String, String>)docMatch.get("docFields");
                    Map<String,String> queryMatches = (Map<String, String>)docMatch.get("queryMatches");
                  */  Iterator<String> it = matchedDoc.getQueryIds().iterator();
                    while (it.hasNext()) {
						String queryId = (String) it.next();
						
						logger.info(" Document "+key+" matched query "+queryId);
						
					}
				} catch (Exception e) {
					logger.error("Failed to recover matched doc",e);
				}
            	 
              /*  Matches<QueryMatch> matches = match((String) message.getKey(), (Map<String, String>) message.getMessage());
                String key = (String)message.getKey();// + "_" + message.getSystemStreamPartition().getPartition().getPartitionId();
                DocumentMatch docMatch = new DocumentMatch(matches, (Map<String, String>) message.getMessage());
                collector.send(new OutgoingMessageEnvelope(MATCHES_STREAM, message.getKey(), key, docMatch));
            */    break;
            default:
                throw new RuntimeException("Unknown stream: " + stream);
        }
      

    }

    private List<Map<String, String>> combineMatches(Map<String, Map<String, String>> parts) {
        return Lists.newArrayList(parts.values());
    }

    @Override
    public void init(Config config, TaskContext taskContext) throws Exception {
        this.store = (KeyValueStore<String, String>) taskContext.getStore("queries");
        this.mapper = new ObjectMapper();
    }

    /*private Map<String, Map<String, String>> collectMatches(String key) {
        Map<String, Map<String, String>> parts = new HashMap<>();
      //  KeyValueIterator<String, Map<String, String>> it = store.range(key + "_", key + "_a");
        try {
            while (it.hasNext()) {
                Entry<String, Map<String, String>> entry = it.next();
                parts.put(entry.getKey(), entry.getValue());
            }
            logger.info("Found {} partial matches in store for {}", parts.size(), key);
            return parts;
        }
        finally {
            it.close();
        }

    }*/

    private String originalKey(String key) {
        return key.replaceAll("_.*?$", "");
    }
}
